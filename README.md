# Quantum X

An extended version of the default [Quantum template](https://quantencomputer.github.io/cyoastudio/templates/quantum.cyoatemplate) for [CYOA Studio](https://quantencomputer.github.io/cyoastudio), with extra features.


## Usage

In addition to [default functionality](https://quantencomputer.github.io/cyoastudio/manual.html), the following tweaks were added to this template specifically:

* Adding `gain` to the _Style classes_ of an option will replace the default **Cost:** prefix with **Gain:**
* Adding `mycost` to the _Style classes_ of an option will remove the default **Cost:** prefix altogether

Reminder: if you need to use multiple _Style classes_, separate them with spaces.


## Download

Latest build can be found [here](https://agregen.gitlab.io/quantum-x/quantumX.cyoatemplate).

For official releases see [release history](https://gitlab.com/agregen/quantum-x/-/releases).
